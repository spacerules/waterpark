package com.keplergaming.spacerules.waterpark;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;


import com.keplergaming.spacerules.blocks.WaterEdge;
import com.keplergaming.spacerules.blocks.WaterJet;
import com.keplergaming.spacerules.entity.EntityWaterJet;
import com.keplergaming.spacerules.item.ItemWaterEdge;
import com.keplergaming.spacerules.proxy.CommonProxy;
import com.keplergaming.spacerules.tabs.WaterParkTab;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.TickRegistry;

/**
 * @author Spacerules
 *
 */
@Mod(modid="WaterPark", name="WaterPark", version="1.5.1.1")
@NetworkMod(clientSideRequired=true, serverSideRequired=false)
/**
 * @author Spacerules
 * @version 1.5.1.1
 */
public class WaterPark {

	// The instance of your mod that Forge uses.
	@Instance("WaterPark")
	public static WaterPark instance;
	
	public static int waterEdgeID=2451; 
	public static Block waterEdge;
	public static String[] WaterEdgeNames = {"White Water Edge", "Orange Water Edge", "Purple Water Edge", "Light Blue Water Edge", "Yellow Water Edge", "Green Water Edge",
		"Pink Water Edge", "Dark Grey Water Edge", "Light Grey Water Edge", "Blue Green Water Edge", "Dark Purple Water Edge", "Dark Blue Water Edge",
		"Brown Water Edge", "Dark Green Water Edge", "Dark Green Water Edge", "Black Water Edge"};
	
	public static int waterJetID=2452;
	public static Block waterJet;
	
	
	public static CreativeTabs waterParkTab;

	// Says where the client and server 'proxy' code is loaded.
	@SidedProxy(clientSide="com.keplergaming.spacerules.proxy.ClientProxy", serverSide="com.keplergaming.spacerules.proxy.CommonProxy")
	public static CommonProxy proxy;

	@PreInit
	public void preInit(FMLPreInitializationEvent event) {
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		
		config.getBlock("Water Edge", waterEdgeID).getInt(waterEdgeID);
		config.getBlock("Water Jet", waterJetID).getInt(waterJetID);
		
		config.save();
		
		waterParkTab = new WaterParkTab(CreativeTabs.getNextID(),"Water Park");
		waterEdge = new WaterEdge(waterEdgeID);
		waterJet = new WaterJet(waterJetID);
	}

	@Init
	public void load(FMLInitializationEvent event) {
		proxy.registerRenderers();
		addWaterEdge();
		addWaterJet();
	}

	private void addWaterJet() {
		GameRegistry.registerTileEntity(EntityWaterJet.class, "Water Jet");
		GameRegistry.registerBlock(waterJet, "Water Jet");
		LanguageRegistry.addName(waterJet, "Water Jet");
	}

	@PostInit
	public void postInit(FMLPostInitializationEvent event) {
		// Stub Method
	}

	
	/**
	 * This method is strictly to add the water edge pieces to the game
	 */
	public void addWaterEdge()
	{
		GameRegistry.registerBlock(waterEdge, ItemWaterEdge.class, "Water Edge");

		for (int ix = 0; ix < 16; ix++) {
			//ItemStack cloth = new ItemStack(Block.cloth, 1, ix);
			ItemStack multiBlockStack = new ItemStack(waterEdge, 1, ix);
			
			//GameRegistry.addShapelessRecipe(multiBlockStack, cloth, cloth);
			LanguageRegistry.addName(multiBlockStack, WaterEdgeNames[multiBlockStack.getItemDamage()]);
		}


	}
}