package com.keplergaming.spacerules.entity;

import java.util.List;

import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;

public class EntityWaterJet extends TileEntity {
	
	public EntityWaterJet()
	{
	}
	
	@Override
	public void updateEntity()
	{
		AxisAlignedBB bb = INFINITE_EXTENT_AABB;
		
		List<EntityBoat> l = this.worldObj.getEntitiesWithinAABB(EntityBoat.class,bb.getAABBPool().getAABB(xCoord-5, yCoord-5, zCoord-5, xCoord+5, yCoord+5, zCoord+5));
		if (!l.isEmpty())
		{
			for(EntityBoat boat: l)
			{
				boat.setVelocity(-1,0,-1);
			}
		}
		
	}
}
