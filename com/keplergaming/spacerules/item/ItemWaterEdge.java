package com.keplergaming.spacerules.item;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

/**
 * @author Spacerules
 * @version 1.5.1.1
 */
public class ItemWaterEdge extends ItemBlock {

	public ItemWaterEdge(int par1) {
		super(par1);
		this.setUnlocalizedName("WaterEdge");
		//this.setCreativeTab(WaterPark.waterParkTab)
		setHasSubtypes(true);
	}

	@Override
	public int getMetadata(int par1) {
		return par1;
	}



	@Override
	public String getUnlocalizedName(ItemStack itemstack)
	{

		String name = "";
		switch (itemstack.getItemDamage())
		{
		case 0:
			name = "White";
			break;
		case 1:
			name = "Orange";
			break;
		case 2:
			name = "Purple";
			break;
		case 3:
			name = "LightBlue";
			break;
		case 4:
			name = "Yellow";
			break;
		case 5:
			name = "Green";
			break;
		case 6:
			name = "Pink";
			break;
		case 7:
			name = "DarkGrey";
			break;
		case 8:
			name = "LightGrey";
			break;
		case 9:
			name = "BlueGreen";
			break;
		case 10:
			name = "DarkPurple";
			break;
		case 11:
			name = "DarkBlue";
			break;
		case 12:
			name = "Brown";
			break;
		case 13:
			name = "DarkGreen";
			break;
		case 14:
			name = "DarkRed";
			break;
		case 15:
			name = "Black";
			break;
		default:
			name = "Black";
		}
		return this.getUnlocalizedName() + "." + name ;
	}

}
