package com.keplergaming.spacerules.render;

import org.lwjgl.opengl.GL11;
import com.keplergaming.spacerules.proxy.ClientProxy;
import com.keplergaming.spacerules.waterpark.WaterPark;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.world.IBlockAccess;

/**
 * @author Spacerules
 * @version 1.5.1.1
 */
public class RenderWaterEdge implements ISimpleBlockRenderingHandler
{	
	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer)
	{
		Tessellator tessellator = Tessellator.instance;
		if(modelID == ClientProxy.renderWaterEdgeID)
		{              
			for(int i = 1; i <= 4; ++i)
			{
				switch(i)
				{
				case 1: renderer.setRenderBounds(0.0D, 0.4D, 0.4D, 0.4D, 0.6D, 0.6D); break;
				case 2: renderer.setRenderBounds(0.6D, 0.4D, 0.4D, 1D, 0.6D, 0.6D); break;
				case 3: renderer.setRenderBounds(0.4D, 0.4D, 0D, 0.6D, 0.6D, 1D); break;
				case 4: renderer.setRenderBounds(0.0D, 0.0D, 0D, 1.0D, 0.4D, 1D); 
				renderer.setOverrideBlockTexture(Block.waterStill.getBlockTextureFromSide(0));
				break;
				}
				block.setBlockBoundsForItemRender();
				GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
				float var7 = 0.0F;
				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, -1.0F, 0.0F);
				renderer.renderBottomFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(0,metadata));
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, 1.0F, 0.0F);
				renderer.renderTopFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(1,metadata));
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, 0.0F, -1.0F);
				tessellator.addTranslation(0.0F, 0.0F, var7);
				renderer.renderEastFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(2,metadata));
				tessellator.addTranslation(0.0F, 0.0F, -var7);
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, 0.0F, 1.0F);
				tessellator.addTranslation(0.0F, 0.0F, -var7);
				renderer.renderWestFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(3,metadata));
				tessellator.addTranslation(0.0F, 0.0F, var7);
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(-1.0F, 0.0F, 0.0F);
				tessellator.addTranslation(var7, 0.0F, 0.0F);
				renderer.renderNorthFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(4,metadata));
				tessellator.addTranslation(-var7, 0.0F, 0.0F);
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(1.0F, 0.0F, 0.0F);
				tessellator.addTranslation(-var7, 0.0F, 0.0F);
				renderer.renderSouthFace(block, 0.0D, 0.0D, 0.0D, block.getBlockTextureFromSideAndMetadata(5,metadata));
				tessellator.addTranslation(var7, 0.0F, 0.0F);
				tessellator.draw();
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				renderer.clearOverrideBlockTexture();
			}
		}
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		//mode key: 0-solo, 1 - x is less, 2-x is more,4 - z is less, 8 - z is more
		int mode = 0;

		if (world.getBlockId(x-1, y, z) == WaterPark.waterEdgeID)
			mode += 1;
		if (world.getBlockId(x+1, y, z) == WaterPark.waterEdgeID)
			mode += 2;
		if (world.getBlockId(x, y, z-1) == WaterPark.waterEdgeID)
			mode += 4;
		if (world.getBlockId(x, y, z+1) == WaterPark.waterEdgeID)
			mode += 8;



		if (mode % 16  >= 8)
		{
			renderer.setRenderBounds(.4, 0, .6, .6, .2, 1);
			renderer.renderStandardBlock(block, x, y, z);
		}
		if (mode % 8  >= 4)
		{
			renderer.setRenderBounds(.4, 0, 0, .6, .2, .4);
			renderer.renderStandardBlock(block, x, y, z);
		}
		if (mode % 4  >= 2)
		{
			renderer.setRenderBounds(.6, 0, .4, 1, .2, .6);
			renderer.renderStandardBlock(block, x, y, z);
		}
		if (mode % 2 == 1)
		{
			renderer.setRenderBounds(0, 0, .4, .4, .2, .6);
			renderer.renderStandardBlock(block, x, y, z);
		}
		renderer.setRenderBounds(.4, 0, .4, .6, .2, .6);
		renderer.renderStandardBlock(block, x, y, z);



		// Your renderring code
		return false;
	}

	@Override
	public boolean shouldRender3DInInventory()
	{
		// This is where it asks if you want the renderInventory part called or not.
		return true; // Change to 'true' if you want the Inventory render to be called.
	}

	@Override
	public int getRenderId()
	{
		// This is one place we need that renderId from earlier.
		return ClientProxy.renderWaterEdgeID;
	}
}