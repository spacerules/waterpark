package com.keplergaming.spacerules.proxy;

/**
 * @author Spacerules
 * @version 1.5.1.1
 */
public class CommonProxy {

	/**
	 * RegisterRenderers is currently not used because it is for the 
	 * 	server and the server doesn't render graphics
	 */
	public void registerRenderers() {

		// Nothing here as the server doesn't render graphics!
	}
}