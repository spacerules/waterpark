package com.keplergaming.spacerules.proxy;


import com.keplergaming.spacerules.render.RenderWaterEdge;

import cpw.mods.fml.client.registry.RenderingRegistry;

/**
 * @author Spacerules
 * 
 */
public class ClientProxy extends CommonProxy {
       
	public static int renderWaterEdgeID;
	
	/** 
	* @see com.keplergaming.spacerules.proxy.CommonProxy#registerRenderers()
	* 
	* registers the rendering for WaterEdgeBlocks
	**/
    @Override
    public void registerRenderers()
    {
    	renderWaterEdgeID = RenderingRegistry.getNextAvailableRenderId();
    	RenderingRegistry.registerBlockHandler(renderWaterEdgeID,new RenderWaterEdge());
    }
       
}