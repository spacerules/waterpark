package com.keplergaming.spacerules.tabs;

import com.keplergaming.spacerules.waterpark.WaterPark;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.creativetab.CreativeTabs;

/**
 * @author Spacerules
 * @version 1.5.1.1
 */
public class WaterParkTab extends CreativeTabs {

	public WaterParkTab(int location, String label)
	{
		super(location, label);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getTabLabel()
	{
		return "Water Park";
	}

	@Override
	@SideOnly(Side.CLIENT)
	/**
	 * Gets the translated Label.
	 */
	public String getTranslatedTabLabel()
	{
		return this.getTabLabel();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getTabIconItemIndex()
	{
		return WaterPark.waterEdgeID;
	}


}
