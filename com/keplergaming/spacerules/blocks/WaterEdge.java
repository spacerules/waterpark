package com.keplergaming.spacerules.blocks;

import java.util.List;
import com.keplergaming.spacerules.proxy.ClientProxy;
import com.keplergaming.spacerules.waterpark.WaterPark;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

/**
 * @author Spacerules
 * @version 1.5.1.1
 */
public class WaterEdge extends Block {

	@SideOnly(Side.CLIENT)
	public static Icon[] colors;

	public WaterEdge(int id) {
		super(id, Material.cloth);
		this.setHardness(.3F); //cloth is .8
		this.setResistance(10F); //stone
		this.setStepSound(soundStoneFootstep);
		this.setLightOpacity(0);
		this.setCreativeTab(WaterPark.waterParkTab);
		this.setUnlocalizedName("WaterEdge");
		this.setBlockBounds(0, 0, 0, 1, .2f, 1);
	}
	
	@Override
	public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
	{
		if (par7Entity == null || !(par7Entity instanceof EntityBoat))
		{
			super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
		}
	}
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
	{
		if (entity instanceof EntityBoat)
		{
			EntityBoat boat = (EntityBoat) entity;
			boat.setVelocity(0, 0, 0);

		}
	}

	@Override
	public int getRenderType()
	{
		return ClientProxy.renderWaterEdgeID;
	}
	
	@Override
	public int damageDropped(int metadata)
	{
		return metadata;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public Icon getBlockTextureFromSideAndMetadata(int side, int meta)
	{
		return WaterEdge.colors[meta];
	}

	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		colors = new Icon[16];

		WaterEdge.colors[0] = par1IconRegister.registerIcon("waterpark:WaterEdgeWhite");
		WaterEdge.colors[1] = par1IconRegister.registerIcon("waterpark:WaterEdgeOrange");
		WaterEdge.colors[2] = par1IconRegister.registerIcon("waterpark:WaterEdgePurple");
		WaterEdge.colors[3] = par1IconRegister.registerIcon("waterpark:WaterEdgeLightBlue");
		WaterEdge.colors[4] = par1IconRegister.registerIcon("waterpark:WaterEdgeYellow");
		WaterEdge.colors[5] = par1IconRegister.registerIcon("waterpark:WaterEdgeGreen");
		WaterEdge.colors[6] = par1IconRegister.registerIcon("waterpark:WaterEdgePink");
		WaterEdge.colors[7] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkGrey");
		WaterEdge.colors[8] = par1IconRegister.registerIcon("waterpark:WaterEdgeLightGrey");
		WaterEdge.colors[9] = par1IconRegister.registerIcon("waterpark:WaterEdgeBlueGreen");
		WaterEdge.colors[10] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkPurple");
		WaterEdge.colors[11] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkBlue");
		WaterEdge.colors[12] = par1IconRegister.registerIcon("waterpark:WaterEdgeBrown");
		WaterEdge.colors[13] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkGreen");
		WaterEdge.colors[14] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkRed");
		WaterEdge.colors[15] = par1IconRegister.registerIcon("waterpark:WaterEdgeBlack");

	}
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(int par1,CreativeTabs par2CreativeTabs,List par3List)
	{

		for(int var4 = 0;var4 < 16;var4++)
		{
			ItemStack temp = new ItemStack(par1,1,var4);
			par3List.add(temp);
		}
	}

}
