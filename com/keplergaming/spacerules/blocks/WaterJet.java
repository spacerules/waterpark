package com.keplergaming.spacerules.blocks;

import java.util.List;
import java.util.Random;

import com.keplergaming.spacerules.entity.EntityWaterJet;
import com.keplergaming.spacerules.proxy.ClientProxy;
import com.keplergaming.spacerules.waterpark.WaterPark;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

/**
 * @author Spacerules
 * @version 1.5.1.1
 */
public class WaterJet extends BlockContainer {

	@SideOnly(Side.CLIENT)
	public static Icon[] colors;

	public WaterJet(int id) {
		super(id, Material.cloth);
		this.setHardness(.3F); //cloth is .8
		this.setResistance(10F); //stone
		this.setStepSound(soundStoneFootstep);
		this.setLightOpacity(0);
		this.setCreativeTab(WaterPark.waterParkTab);
		this.setUnlocalizedName("WaterJet");
		//this.setTickRandomly(true);
		this.setBlockBounds(0, 0, 0, 1, .2f, 1);
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLiving entity, ItemStack itemStack)
    {
        int l = MathHelper.floor_double((double)(entity.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

        if (l == 0)
        {
            world.setBlockMetadataWithNotify(x, y, z, 2, 2);
        }

        if (l == 1)
        {
            world.setBlockMetadataWithNotify(x, y, z, 5, 2);
        }

        if (l == 2)
        {
            world.setBlockMetadataWithNotify(x, y, z, 3, 2);
        }

        if (l == 3)
        {
            world.setBlockMetadataWithNotify(x, y, z, 4, 2);
        }
    }
	
	
	@Override
	public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
	{
		if (par7Entity == null || !(par7Entity instanceof EntityBoat))
		{
			super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
		}
	}
	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
	{
		if (entity instanceof EntityBoat)
		{
			EntityBoat boat = (EntityBoat) entity;
			boat.setVelocity(0, 0, 0);

		}
	}

	@Override
	public int getRenderType()
	{
		return ClientProxy.renderWaterEdgeID;
	}
	
	@Override
	public int damageDropped(int metadata)
	{
		return metadata;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public Icon getBlockTextureFromSideAndMetadata(int side, int meta)
	{
		return WaterJet.colors[meta];
	}

	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		colors = new Icon[16];
		WaterJet.colors[0] = par1IconRegister.registerIcon("waterpark:WaterEdgeWhite");
		WaterJet.colors[1] = par1IconRegister.registerIcon("waterpark:WaterEdgeOrange");
		WaterJet.colors[2] = par1IconRegister.registerIcon("waterpark:WaterEdgePurple");
		WaterJet.colors[3] = par1IconRegister.registerIcon("waterpark:WaterEdgeLightBlue");
		WaterJet.colors[4] = par1IconRegister.registerIcon("waterpark:WaterEdgeYellow");
		WaterJet.colors[5] = par1IconRegister.registerIcon("waterpark:WaterEdgeGreen");
		WaterJet.colors[6] = par1IconRegister.registerIcon("waterpark:WaterEdgePink");
		WaterJet.colors[7] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkGrey");
		WaterJet.colors[8] = par1IconRegister.registerIcon("waterpark:WaterEdgeLightGrey");
		WaterJet.colors[9] = par1IconRegister.registerIcon("waterpark:WaterEdgeBlueGreen");
		WaterJet.colors[10] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkPurple");
		WaterJet.colors[11] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkBlue");
		WaterJet.colors[12] = par1IconRegister.registerIcon("waterpark:WaterEdgeBrown");
		WaterJet.colors[13] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkGreen");
		WaterJet.colors[14] = par1IconRegister.registerIcon("waterpark:WaterEdgeDarkRed");
		WaterJet.colors[15] = par1IconRegister.registerIcon("waterpark:WaterEdgeBlack");

	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new EntityWaterJet();
	}

}
